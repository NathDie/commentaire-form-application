import './css/App.css'

import Home from './pages/Home'
import Commentaires from "./pages/Commentaires";
import Navbar from './Navbar'

import Users from './pages/Users'
import UserDetails from './pages/UserDetails'

import { Routes, Route } from "react-router-dom";

function App() {
  return (
      <div className="App">
          <Navbar />
          <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/commentaires" element={<Commentaires/>} />
              <Route path="/users" element={<Users/>} />
              <Route path="/users/:userid" element={<UserDetails/>} />
          </Routes>
      </div>
  );
}

export default App;
