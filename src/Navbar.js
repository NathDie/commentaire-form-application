import { NavLink } from "react-router-dom";
import './css/Navbar.css';

export default function Navbar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <NavLink to="/" className="nav-link">Accueil</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/users" className="nav-link">Utilisateurs</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/commentaires" className="nav-link">Commentaires</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}