import axios from "axios"
import { useEffect, useState } from "react"
import {Link} from "react-router-dom"

export default function Users() {
    const [users,setUsers] = useState([])

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                setUsers(res.data)
            })
    }, [])

    return (
        <div className="container">
            <div className="row">
                <h1>Liste de mes utilisateurs</h1>
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">Pseudo</th>
                        <th scope="col">Company</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    {users.map(user => {
                        return     <tbody>
                        <tr>
                            <td>{user.name}</td>
                            <td>{user.company.name}</td>
                            <td><Link to={"/users/" + user.id}>Voir</Link></td>
                        </tr>
                        </tbody>
                    })}
                </table>
            </div>
        </div>
    )
}