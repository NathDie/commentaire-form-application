import {useParams, NavLink} from "react-router-dom";
import axios from "axios";
import {useEffect, useState} from "react";
import '../css/userdetails.css';

export default function UserDetails() {
    const [user,setUser] = useState([])

    let params = useParams();
    let userId = params.userid;

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(res => {
                setUser(res.data)
                console.log(res.data)
            })
    }, [])

    if(user.length === 0) {
        return (<span>Loading...</span>)
    }
    return (
        <div className="container">
            <NavLink to="/users" className="nav-link return-link">Retour en arriere ></NavLink>
            <h1 className="mb-5">A propos de l'utilisateur : {user.name}</h1>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-header text-center font-weight-bold">
                            Information's personnel
                        </div>
                        <ul className="list-group list-group-flush text-center mt-2">
                            <p>Nom : {user.name}</p>
                            <p>Email : {user.email}</p>
                            <p>Username : {user.username}</p>
                            <p>Website : {user.website}</p>
                        </ul>
                    </div>
                </div>
                <div className="col">
                    <div className="card">
                        <div className="card-header text-center font-weight-bold">
                            Address personnel
                        </div>
                        <ul className="list-group list-group-flush text-center mt-2">
                            <p>Street : {user.address.street}</p>
                            <p>Suite : {user.address.suite}</p>
                            <p>City : {user.address.city}</p>
                            <p>ZipCode : {user.address.zipcode}</p>
                        </ul>
                    </div>
                </div>
                <div className="col">
                    <div className="card">
                        <div className="card-header text-center font-weight-bold">
                            Company personnel
                        </div>
                        <ul className="list-group list-group-flush text-center mt-2">
                            <p>Name : {user.company.name}</p>
                            <p>CatchPhrase : {user.company.catchPhrase}</p>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}