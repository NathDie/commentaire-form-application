import { useState } from "react"
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Commentaires() {
    const flex = {
        display:'flex'
    };

    const [users, setUsers] = useState([
        {pseudo: "Kirikou", message: "Je ne suis pas grand mais je suis vaillant"},
        {pseudo: "Jean-Claude", message: "Oublies que t'as aucune chance, vas-y fonce!"}
    ])

    const [pseudo, setPseudo] = useState("")
    const [message, setMessage] = useState("")

    function handlePseudo(event) {
        setPseudo(event.target.value)
    }

    function handleMessage(event) {
        setMessage(event.target.value)
    }

    function handleSubmit(event) {
        event.preventDefault();
        console.log("J'ai soumis le formulaire")

        const newUser = {pseudo: pseudo, message: message}
        console.log(newUser);

        // ...users -> ça récupère chacun des éléments de users,
        // et ça les met à la suite dans le nouveau tableau
        // ,newUser : ajoute à la fin notre nouvel utilisateur
        setUsers([...users, newUser]);
    }

    return (
        <div className="form">
            <div className="container">
                <div className="row mt-3">
                    <div className="col">
                        <h2>Ajouter un commentaire</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <input className="form-control" type="text" value={pseudo} onChange={handlePseudo} placeholder="Entrez votre pseudo"/>
                            </div>
                            <div className="form-group mt-2 mb-2">
                                <textarea className="form-control" value={message} onChange={handleMessage} placeholder="Entrez votre commentaire"/>
                            </div>
                            <button type="submit" className="btn btn-primary text-center form-control">Envoyer</button>
                        </form>
                    </div>
                    <div className="col">
                        <h2>Commentaires ({users.length})</h2>
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Pseudo</th>
                                <th scope="col">Message</th>
                            </tr>
                            </thead>
                            {users.map(user => {
                                return     <tbody>
                                <tr>
                                    <td>{user.pseudo}</td>
                                    <td>{user.message}</td>
                                </tr>
                                </tbody>
                            })}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}